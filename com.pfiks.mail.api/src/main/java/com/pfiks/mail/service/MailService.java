/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.mail.service;

import java.io.File;
import java.util.List;

import com.liferay.portal.kernel.model.User;
import com.pfiks.mail.exception.MailException;

public interface MailService {

	/**
	 * Sends an email
	 *
	 * @param toAddress the recipient's email address
	 * @param toName the recipient's name
	 * @param fromAddress the sender's email address
	 * @param fromName the sender's name
	 * @param subject the subject
	 * @param body the body
	 * @throws MailException if any exception occurs
	 */
	public void sendEmail(String toAddress, String toName, String fromAddress, String fromName, String subject, String body) throws MailException;

	/**
	 * Sends an email with attachments
	 *
	 * @param toAddress the recipient's email address
	 * @param toName the recipient's name
	 * @param fromAddress the sender's email address
	 * @param fromName the sender's name
	 * @param subject the subject
	 * @param body the body
	 * @param attachments a list of files to attach
	 * @throws MailException if any exception occurs
	 */

	public void sendEmail(String toAddress, String toName, String fromAddress, String fromName, String subject, String body, List<File> attachments) throws MailException;

	/**
	 * Sends an email
	 *
	 * @param recipientUser the recipient user
	 * @param fromAddress the sender's email address
	 * @param fromName the sender's name
	 * @param subject the subject
	 * @param body the body
	 * @throws MailException if any exception occurs
	 */
	public void sendEmail(User recipientUser, String fromAddress, String fromName, String subject, String body) throws MailException;

	/**
	 * Sends an email to the recipient from default admin account
	 *
	 * @param toAddress the receiver's email
	 * @param toName the receiver's name
	 * @param subject email's subject
	 * @param body emai's body
	 * @throws MailException if any exception occurs
	 */
	public void sendEmail(String toAddress, String toName, String subject, String body) throws MailException;

	/**
	 * Sends an email with attachments to the recipient from default admin
	 * account
	 *
	 * @param toAddress the receiver's email
	 * @param toName the receiver's name
	 * @param subject email's subject
	 * @param body emai's body
	 * @param attachments a list of files to attach
	 * @throws MailException if any exception occurs
	 */
	public void sendEmail(String toAddress, String toName, String subject, String body, List<File> attachments) throws MailException;

	/**
	 * Sends an email to the recipient from instance email configured
	 *
	 * @param companyId the companyId
	 * @param toAddress the receiver's email
	 * @param toName the receiver's name
	 * @param subject email's subject
	 * @param body emai's body
	 * @throws MailException if any exception occurs
	 */
	public void sendEmail(long companyId, String toAddress, String toName, String subject, String body) throws MailException;

	/**
	 * Sends an email to the recipient from default admin account
	 *
	 * @param recipientUser the recipient user
	 * @param subject email's subject
	 * @param body emai's body
	 * @throws MailException if any exception occurs
	 */
	public void sendEmail(User recipientUser, String subject, String body) throws MailException;

}
