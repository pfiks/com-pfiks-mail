/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.mail.exception;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class MailExceptionTest {

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public final void mailException_ShouldBeThrown_WithMessageAndThrowable() throws MailException {
		thrown.expect(MailException.class);
		thrown.expectMessage("message");
		throw new MailException("message", new Exception());
	}

	@Test
	public final void mailException_ShouldBeThrown_WithThrowable() throws MailException {
		thrown.expect(MailException.class);
		throw new MailException(new Exception());
	}

	@Test
	public final void mailException_ShouldBeThrown_WithoutMessage() throws MailException {
		thrown.expectMessage("message");
		throw new MailException("message");
	}
}
