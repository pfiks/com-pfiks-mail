/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.mail.template.contributor.freemarker;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.template.TemplateContextContributor;
import com.pfiks.mail.service.MailService;

@Component(immediate = true, property = "type=" + TemplateContextContributor.TYPE_GLOBAL, service = TemplateContextContributor.class)
public class DigitalPlaceMailFreemarkerTemplateContextContributor implements TemplateContextContributor {

	public static final String DIGITALPLACE_MAIL_SERVICE = "digitalplace_mailService";

	@Reference
	private MailService mailService;

	@Override
	public void prepare(Map<String, Object> contextObjects, HttpServletRequest httpServletRequest) {
		contextObjects.put(DIGITALPLACE_MAIL_SERVICE, mailService);
	}

}
