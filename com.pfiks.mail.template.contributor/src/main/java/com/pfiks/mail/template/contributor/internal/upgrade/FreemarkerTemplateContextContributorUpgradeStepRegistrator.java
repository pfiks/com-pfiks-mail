/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.mail.template.contributor.internal.upgrade;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.fragment.service.FragmentEntryLocalService;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.pfiks.mail.template.contributor.internal.upgrade.upgrade_1_0_0.DDMTemplateUpgradeProcess;
import com.pfiks.mail.template.contributor.internal.upgrade.upgrade_1_0_0.FragmentsUpgradeProcess;

@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class FreemarkerTemplateContextContributorUpgradeStepRegistrator implements UpgradeStepRegistrator {

	@Reference
	private CompanyLocalService companyLocalService;

	@Reference
	private DDMTemplateLocalService ddmTemplateLocalService;

	@Reference
	private FragmentEntryLocalService fragmentEntryLocalService;

	@Override
	public void register(Registry registry) {
		registry.register("0.0.0", "1.0.0", new DDMTemplateUpgradeProcess(companyLocalService, ddmTemplateLocalService), new FragmentsUpgradeProcess(companyLocalService, fragmentEntryLocalService));
	}

}
