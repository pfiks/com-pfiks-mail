/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.mail.template.contributor.internal.util;

import static com.pfiks.mail.template.contributor.freemarker.DigitalPlaceMailFreemarkerTemplateContextContributor.DIGITALPLACE_MAIL_SERVICE;

import java.io.File;
import java.io.IOException;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.FileUtil;
import com.pfiks.mail.service.MailService;

public class UpgradeHelper {

	private UpgradeHelper() {
	}

	public static File getOutputFile(UpgradeProcess upgradeProcess) {
		return new File(System.getProperty("liferay.home") + "/upgrade/" + upgradeProcess.getClass().getName() + ".txt");
	}

	public static boolean hasComponent(String script) {
		return script.contains(MailService.class.getName());
	}

	public static String upgradeScript(String script) {
		return replaceServiceLocatorCallByReference(script, MailService.class, DIGITALPLACE_MAIL_SERVICE);
	}

	public static void writeOutput(File file, String output) throws IOException {
		FileUtil.write(file, output, false, true);
	}

	private static String replaceServiceLocatorCallByReference(String script, Class clazz, String reference) {
		return script.replace("serviceLocator.findService(\"" + clazz.getName() + "\")", reference);
	}

}
