/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.mail.util;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

import java.io.UnsupportedEncodingException;

import javax.mail.internet.InternetAddress;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.liferay.mail.kernel.model.MailMessage;
import com.pfiks.mail.util.MailFactoryUtil;

public class MailFactoryUtilTest {

	private MailFactoryUtil notificationMailFactory = new MailFactoryUtil();

	@Mock
	private InternetAddress mockInternetAddressTo;

	@Mock
	private InternetAddress mockInternetAddressFrom;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getInternetAddress_WhenNoErrors_ThenReturnsNewInternetAddress() throws UnsupportedEncodingException {
		String address = "test@test.com";
		String name = "nameValue";

		InternetAddress result = notificationMailFactory.getInternetAddress(address, name);

		assertThat(result.getAddress(), equalTo(address));
		assertThat(result.getPersonal(), equalTo(name));
	}

	@Test
	public void getMailMessage_WhenNoErrors_ThenReturnsNewMailMessage() {
		String body = "bodyValue";
		String subject = "subjectValue";

		MailMessage result = notificationMailFactory.getMailMessage(mockInternetAddressFrom, mockInternetAddressTo, subject, body);

		assertThat(result.getFrom(), sameInstance(mockInternetAddressFrom));
		assertThat(result.getTo()[0], sameInstance(mockInternetAddressTo));
		assertThat(result.getSubject(), equalTo(subject));
		assertThat(result.getBody(), equalTo(body));
		assertThat(result.getHTMLFormat(), equalTo(true));
	}

}
