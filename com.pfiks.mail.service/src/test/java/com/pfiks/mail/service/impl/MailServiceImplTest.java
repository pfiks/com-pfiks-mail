/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.mail.service.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.InternetAddress;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.mail.kernel.model.MailMessage;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.pfiks.mail.exception.MailException;
import com.pfiks.mail.util.MailFactoryUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ CompanyThreadLocal.class, PrefsPropsUtil.class })
public class MailServiceImplTest {

	private MailServiceImpl mailServiceImpl;

	private static final long COMPANY_ID = 1;
	private static final String FROM_CUSTOM_ADDRESS = "test@custom.com";
	private static final String FROM_CUSTOM_NAME = "Joe Custom";
	private static final String FROM_SYSTEM_ADDRESS = "Admin@liferay.com";
	private static final String FROM_SYSTEM_NAME = "Admin";
	private static final String RECIPIENT_ADDRESS = "test@test.com";
	private static final String RECIPIENT_NAME = "Test";
	private static final String SUBJECT = "subject";
	private static final String BODY = "body";

	@Mock
	private com.liferay.mail.kernel.service.MailService mockMailService;

	@Mock
	private MailFactoryUtil mockMailFactoryUtil;

	@Mock
	private InternetAddress mockToInternetAddress;

	@Mock
	private InternetAddress mockFromInternetAddress;

	@Mock
	private InternetAddress mockFromAdminInternetAddress;

	@Mock
	private MailMessage mockMailMessage;

	@Mock
	private User mockUser;

	@Mock
	private File mockFile;

	@Before
	public void setUp() {
		initMocks(this);
		mockStatic(CompanyThreadLocal.class, PrefsPropsUtil.class);

		mailServiceImpl = new MailServiceImpl();
		mailServiceImpl.setMailService(mockMailService);
		mailServiceImpl.setMailFactoryUtil(mockMailFactoryUtil);
	}

	@Test(expected = MailException.class)
	public void sendEmail_WithToAddressAndToNameAndFromAddressAndFromNameAndSubjectAndBodyParameters_WhenError_ThenMailExceptionIsThrown() throws Exception {
		when(mockMailFactoryUtil.getInternetAddress(anyString(), anyString())).thenThrow(new UnsupportedEncodingException());

		mailServiceImpl.sendEmail(anyString(), anyString(), anyString(), anyString(), anyString(), anyString());
	}

	@Test
	public void sendEmail_WithToAddressAndToNameAndFromAddressAndFromNameAndSubjectAndBodyParameters_WhenNoError_ThenEmailIsSent() throws Exception {
		when(mockMailFactoryUtil.getInternetAddress(RECIPIENT_ADDRESS, RECIPIENT_NAME)).thenReturn(mockToInternetAddress);
		when(mockMailFactoryUtil.getInternetAddress(FROM_CUSTOM_ADDRESS, FROM_CUSTOM_NAME)).thenReturn(mockFromInternetAddress);
		when(mockMailFactoryUtil.getMailMessage(mockFromInternetAddress, mockToInternetAddress, SUBJECT, BODY)).thenReturn(mockMailMessage);

		mailServiceImpl.sendEmail(RECIPIENT_ADDRESS, RECIPIENT_NAME, FROM_CUSTOM_ADDRESS, FROM_CUSTOM_NAME, SUBJECT, BODY);

		verify(mockMailService, times(1)).sendEmail(mockMailMessage);
	}

	@Test
	public void sendEmail_WithToAddressAndToNameAndFromAddressAndFromNameAndSubjectAndBodyParametersAndAttachmentsAreNull_ThenEmailIsSent() throws Exception {
		when(mockMailFactoryUtil.getInternetAddress(RECIPIENT_ADDRESS, RECIPIENT_NAME)).thenReturn(mockToInternetAddress);
		when(mockMailFactoryUtil.getInternetAddress(FROM_CUSTOM_ADDRESS, FROM_CUSTOM_NAME)).thenReturn(mockFromInternetAddress);
		when(mockMailFactoryUtil.getMailMessage(mockFromInternetAddress, mockToInternetAddress, SUBJECT, BODY)).thenReturn(mockMailMessage);

		mailServiceImpl.sendEmail(RECIPIENT_ADDRESS, RECIPIENT_NAME, FROM_CUSTOM_ADDRESS, FROM_CUSTOM_NAME, SUBJECT, BODY, null);

		verify(mockMailService, times(1)).sendEmail(mockMailMessage);
	}

	@Test
	public void sendEmail_WithToAddressAndToNameAndFromAddressAndFromNameAndSubjectAndBodyParametersAndNoAttachments_ThenNoAttachmentIsAdded() throws Exception {
		when(mockMailFactoryUtil.getInternetAddress(RECIPIENT_ADDRESS, RECIPIENT_NAME)).thenReturn(mockToInternetAddress);
		when(mockMailFactoryUtil.getInternetAddress(FROM_CUSTOM_ADDRESS, FROM_CUSTOM_NAME)).thenReturn(mockFromInternetAddress);
		when(mockMailFactoryUtil.getMailMessage(mockFromInternetAddress, mockToInternetAddress, SUBJECT, BODY)).thenReturn(mockMailMessage);

		List<File> attachments = new ArrayList<>();

		mailServiceImpl.sendEmail(RECIPIENT_ADDRESS, RECIPIENT_NAME, FROM_CUSTOM_ADDRESS, FROM_CUSTOM_NAME, SUBJECT, BODY, attachments);

		verify(mockMailMessage, never()).addFileAttachment(any(File.class));
	}

	@Test
	public void sendEmail_WithToAddressAndToNameAndFromAddressAndFromNameAndSubjectAndBodyParametersAndHasAnAttachment_ThenAttachmentIsAddedToEmail() throws Exception {
		when(mockMailFactoryUtil.getInternetAddress(RECIPIENT_ADDRESS, RECIPIENT_NAME)).thenReturn(mockToInternetAddress);
		when(mockMailFactoryUtil.getInternetAddress(FROM_CUSTOM_ADDRESS, FROM_CUSTOM_NAME)).thenReturn(mockFromInternetAddress);
		when(mockMailFactoryUtil.getMailMessage(mockFromInternetAddress, mockToInternetAddress, SUBJECT, BODY)).thenReturn(mockMailMessage);

		List<File> attachments = new ArrayList<>();
		attachments.add(mockFile);

		mailServiceImpl.sendEmail(RECIPIENT_ADDRESS, RECIPIENT_NAME, FROM_CUSTOM_ADDRESS, FROM_CUSTOM_NAME, SUBJECT, BODY, attachments);

		verify(mockMailMessage, times(1)).addFileAttachment(mockFile);

	}

	@Test
	public void sendEmail_WithUserAndFromAddressAndFromNameAndSubjectAndBodyParameters_WhenUserIsNull_ThenNoEmailIsSent() throws Exception {
		mailServiceImpl.sendEmail(null, FROM_CUSTOM_ADDRESS, FROM_CUSTOM_NAME, SUBJECT, BODY);

		verifyZeroInteractions(mockMailService);
	}

	@Test
	public void sendEmail_WithUserAndFromAddressAndFromNameAndSubjectAndBodyParameters_WhenUserIsNotActive_ThenNoEmailIsSent() throws Exception {
		when(mockUser.isActive()).thenReturn(false);

		mailServiceImpl.sendEmail(mockUser, FROM_CUSTOM_ADDRESS, FROM_CUSTOM_NAME, SUBJECT, BODY);

		verifyZeroInteractions(mockMailService);
	}

	@Test
	public void sendEmail_WithUserAndFromAddressAndFromNameAndSubjectAndBodyParameters_WhenUserHasNotAcceptedTermsAndConditionsAndTermsAndConditionsAreRequired_ThenNoEmailIsSent() throws Exception {
		when(mockUser.isActive()).thenReturn(false);
		mockTermsAndConditions(false, true);

		mailServiceImpl.sendEmail(mockUser, FROM_CUSTOM_ADDRESS, FROM_CUSTOM_NAME, SUBJECT, BODY);

		verifyZeroInteractions(mockMailService);
	}

	@Test
	public void sendEmail_WithUserAndFromAddressAndFromNameAndSubjectAndBodyParameters_WhenUserIsHasNotAcceptedTermsAndConditionsAndTermsAndConditionsAreNotRequired_ThenEmailIsSent()
			throws Exception {
		when(mockUser.isActive()).thenReturn(true);
		when(mockUser.getEmailAddress()).thenReturn(RECIPIENT_ADDRESS);
		when(mockUser.getFullName()).thenReturn(RECIPIENT_NAME);
		mockTermsAndConditions(false, false);
		when(mockMailFactoryUtil.getInternetAddress(RECIPIENT_ADDRESS, RECIPIENT_NAME)).thenReturn(mockToInternetAddress);
		when(mockMailFactoryUtil.getInternetAddress(FROM_CUSTOM_ADDRESS, FROM_CUSTOM_NAME)).thenReturn(mockFromInternetAddress);
		when(mockMailFactoryUtil.getMailMessage(mockFromInternetAddress, mockToInternetAddress, SUBJECT, BODY)).thenReturn(mockMailMessage);

		mailServiceImpl.sendEmail(mockUser, FROM_CUSTOM_ADDRESS, FROM_CUSTOM_NAME, SUBJECT, BODY);

		verify(mockMailService, times(1)).sendEmail(mockMailMessage);
	}

	@Test
	public void sendEmail_WithUserAndFromAddressAndFromNameAndSubjectAndBodyParameters_WhenUserHasAcceptedTermsAndConditionsAndTermsAndConditionsAreRequired_ThenEmailIsSent() throws Exception {
		when(mockUser.isActive()).thenReturn(true);
		when(mockUser.getEmailAddress()).thenReturn(RECIPIENT_ADDRESS);
		when(mockUser.getFullName()).thenReturn(RECIPIENT_NAME);
		mockTermsAndConditions(true, true);
		when(mockMailFactoryUtil.getInternetAddress(RECIPIENT_ADDRESS, RECIPIENT_NAME)).thenReturn(mockToInternetAddress);
		when(mockMailFactoryUtil.getInternetAddress(FROM_CUSTOM_ADDRESS, FROM_CUSTOM_NAME)).thenReturn(mockFromInternetAddress);
		when(mockMailFactoryUtil.getMailMessage(mockFromInternetAddress, mockToInternetAddress, SUBJECT, BODY)).thenReturn(mockMailMessage);

		mailServiceImpl.sendEmail(mockUser, FROM_CUSTOM_ADDRESS, FROM_CUSTOM_NAME, SUBJECT, BODY);

		verify(mockMailService, times(1)).sendEmail(mockMailMessage);
	}

	@Test
	public void sendEmail_WithUserAndFromAddressAndFromNameAndSubjectAndBodyParameters_WhenValidUser_ThenEmailIsSent() throws Exception {
		when(mockUser.isActive()).thenReturn(true);
		when(mockUser.getEmailAddress()).thenReturn(RECIPIENT_ADDRESS);
		when(mockUser.getFullName()).thenReturn(RECIPIENT_NAME);
		mockTermsAndConditions(false, false);
		when(mockMailFactoryUtil.getInternetAddress(RECIPIENT_ADDRESS, RECIPIENT_NAME)).thenReturn(mockToInternetAddress);
		when(mockMailFactoryUtil.getInternetAddress(FROM_CUSTOM_ADDRESS, FROM_CUSTOM_NAME)).thenReturn(mockFromInternetAddress);
		when(mockMailFactoryUtil.getMailMessage(mockFromInternetAddress, mockToInternetAddress, SUBJECT, BODY)).thenReturn(mockMailMessage);

		mailServiceImpl.sendEmail(mockUser, FROM_CUSTOM_ADDRESS, FROM_CUSTOM_NAME, SUBJECT, BODY);

		verify(mockMailService, times(1)).sendEmail(mockMailMessage);
	}

	@Test
	public void sendEmail_WithToAddressAndToNameAndSubjectAndBodyParameters_WhenNoError_ThenEmailIsSentUsingTheDefaultAdminValues() throws Exception {
		when(CompanyThreadLocal.getCompanyId()).thenReturn(COMPANY_ID);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_NAME)).thenReturn(FROM_SYSTEM_NAME);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_ADDRESS)).thenReturn(FROM_SYSTEM_ADDRESS);
		when(mockMailFactoryUtil.getInternetAddress(RECIPIENT_ADDRESS, RECIPIENT_NAME)).thenReturn(mockToInternetAddress);
		when(mockMailFactoryUtil.getInternetAddress(FROM_SYSTEM_ADDRESS, FROM_SYSTEM_NAME)).thenReturn(mockFromAdminInternetAddress);
		when(mockMailFactoryUtil.getMailMessage(mockFromAdminInternetAddress, mockToInternetAddress, SUBJECT, BODY)).thenReturn(mockMailMessage);

		mailServiceImpl.sendEmail(RECIPIENT_ADDRESS, RECIPIENT_NAME, SUBJECT, BODY);

		verify(mockMailService, times(1)).sendEmail(mockMailMessage);
	}

	@Test
	public void sendEmail_WithToAddressAndToNameAndSubjectAndBodyParametersAndAttachmentsAreNull_ThenEmailIsSent() throws Exception {
		when(CompanyThreadLocal.getCompanyId()).thenReturn(COMPANY_ID);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_NAME)).thenReturn(FROM_SYSTEM_NAME);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_ADDRESS)).thenReturn(FROM_SYSTEM_ADDRESS);
		when(mockMailFactoryUtil.getInternetAddress(RECIPIENT_ADDRESS, RECIPIENT_NAME)).thenReturn(mockToInternetAddress);
		when(mockMailFactoryUtil.getInternetAddress(FROM_SYSTEM_ADDRESS, FROM_SYSTEM_NAME)).thenReturn(mockFromAdminInternetAddress);
		when(mockMailFactoryUtil.getMailMessage(mockFromAdminInternetAddress, mockToInternetAddress, SUBJECT, BODY)).thenReturn(mockMailMessage);

		mailServiceImpl.sendEmail(RECIPIENT_ADDRESS, RECIPIENT_NAME, SUBJECT, BODY, null);

		verify(mockMailService, times(1)).sendEmail(mockMailMessage);
	}

	@Test
	public void sendEmail_WithToAddressAndToNameAndSubjectAndBodyParametersAndNoAttachments_ThenNoAttachmentAreAdded() throws Exception {
		when(CompanyThreadLocal.getCompanyId()).thenReturn(COMPANY_ID);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_NAME)).thenReturn(FROM_SYSTEM_NAME);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_ADDRESS)).thenReturn(FROM_SYSTEM_ADDRESS);
		when(mockMailFactoryUtil.getInternetAddress(RECIPIENT_ADDRESS, RECIPIENT_NAME)).thenReturn(mockToInternetAddress);
		when(mockMailFactoryUtil.getInternetAddress(FROM_SYSTEM_ADDRESS, FROM_SYSTEM_NAME)).thenReturn(mockFromAdminInternetAddress);
		when(mockMailFactoryUtil.getMailMessage(mockFromAdminInternetAddress, mockToInternetAddress, SUBJECT, BODY)).thenReturn(mockMailMessage);

		List<File> attachments = new ArrayList<>();

		mailServiceImpl.sendEmail(RECIPIENT_ADDRESS, RECIPIENT_NAME, SUBJECT, BODY, attachments);

		verify(mockMailMessage, never()).addFileAttachment(any(File.class));
	}

	@Test
	public void sendEmail_WithToAddressAndToNameAndSubjectAndBodyParametersAndHasAnAttachment_ThenAttachmentIsAdded() throws Exception {
		when(CompanyThreadLocal.getCompanyId()).thenReturn(COMPANY_ID);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_NAME)).thenReturn(FROM_SYSTEM_NAME);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_ADDRESS)).thenReturn(FROM_SYSTEM_ADDRESS);
		when(mockMailFactoryUtil.getInternetAddress(RECIPIENT_ADDRESS, RECIPIENT_NAME)).thenReturn(mockToInternetAddress);
		when(mockMailFactoryUtil.getInternetAddress(FROM_SYSTEM_ADDRESS, FROM_SYSTEM_NAME)).thenReturn(mockFromAdminInternetAddress);
		when(mockMailFactoryUtil.getMailMessage(mockFromAdminInternetAddress, mockToInternetAddress, SUBJECT, BODY)).thenReturn(mockMailMessage);

		List<File> attachments = new ArrayList<>();
		attachments.add(mockFile);

		mailServiceImpl.sendEmail(RECIPIENT_ADDRESS, RECIPIENT_NAME, SUBJECT, BODY, attachments);

		verify(mockMailMessage, times(1)).addFileAttachment(mockFile);
	}

	@Test
	public void sendEmail_WithUserAndSubjectAndBodyParameters_WhenUserIsNull_ThenNoEmailIsSent() throws Exception {
		mailServiceImpl.sendEmail(null, SUBJECT, BODY);

		verifyZeroInteractions(mockMailService);
	}

	@Test
	public void sendEmail_WithUserAndSubjectAndBodyParameters_WhenUserIsNotActive_ThenNoEmailIsSent() throws Exception {
		when(mockUser.isActive()).thenReturn(false);

		mailServiceImpl.sendEmail(mockUser, SUBJECT, BODY);

		verifyZeroInteractions(mockMailService);
	}

	@Test
	public void sendEmail_WithUserAndSubjectAndBodyParameters_WhenUserHasNotAcceptedTermsAndConditionsAndTermsAndConditionsAreRequired_ThenNoEmailIsSent() throws Exception {
		when(mockUser.isActive()).thenReturn(false);
		mockTermsAndConditions(false, true);

		mailServiceImpl.sendEmail(mockUser, SUBJECT, BODY);

		verifyZeroInteractions(mockMailService);
	}

	@Test
	public void sendEmail_WithUserAndSubjectAndBodyParameters_WhenUserHasNotAcceptedTermsAndConditionsAndTermsAndConditionsAreNotRequired_ThenEmailIsSentUsingTheDefaultCompanyInstanceEmailValues()
			throws Exception {
		mockTermsAndConditions(false, false);
		when(mockUser.isActive()).thenReturn(true);
		when(mockUser.getEmailAddress()).thenReturn(RECIPIENT_ADDRESS);
		when(mockUser.getFullName()).thenReturn(RECIPIENT_NAME);
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_NAME)).thenReturn(FROM_SYSTEM_NAME);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_ADDRESS)).thenReturn(FROM_SYSTEM_ADDRESS);
		when(mockMailFactoryUtil.getInternetAddress(RECIPIENT_ADDRESS, RECIPIENT_NAME)).thenReturn(mockToInternetAddress);
		when(mockMailFactoryUtil.getInternetAddress(FROM_SYSTEM_ADDRESS, FROM_SYSTEM_NAME)).thenReturn(mockFromAdminInternetAddress);
		when(mockMailFactoryUtil.getMailMessage(mockFromAdminInternetAddress, mockToInternetAddress, SUBJECT, BODY)).thenReturn(mockMailMessage);

		mailServiceImpl.sendEmail(mockUser, SUBJECT, BODY);

		verify(mockMailService, times(1)).sendEmail(mockMailMessage);
	}

	@Test
	public void sendEmail_WithUserAndSubjectAndBodyParameters_WhenUserHasAcceptedTermsAndConditionsAndTermsAndConditionsAreRequired_ThenEmailIsSentUsingTheDefaultCompanyInstanceEmailValues()
			throws Exception {
		mockTermsAndConditions(true, true);
		when(mockUser.isActive()).thenReturn(true);
		when(mockUser.getEmailAddress()).thenReturn(RECIPIENT_ADDRESS);
		when(mockUser.getFullName()).thenReturn(RECIPIENT_NAME);
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_NAME)).thenReturn(FROM_SYSTEM_NAME);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_ADDRESS)).thenReturn(FROM_SYSTEM_ADDRESS);
		when(mockMailFactoryUtil.getInternetAddress(RECIPIENT_ADDRESS, RECIPIENT_NAME)).thenReturn(mockToInternetAddress);
		when(mockMailFactoryUtil.getInternetAddress(FROM_SYSTEM_ADDRESS, FROM_SYSTEM_NAME)).thenReturn(mockFromAdminInternetAddress);
		when(mockMailFactoryUtil.getMailMessage(mockFromAdminInternetAddress, mockToInternetAddress, SUBJECT, BODY)).thenReturn(mockMailMessage);

		mailServiceImpl.sendEmail(mockUser, SUBJECT, BODY);

		verify(mockMailService, times(1)).sendEmail(mockMailMessage);
	}

	@Test
	public void sendEmail_WithUserAndSubjectAndBodyParameters_WhenValidUser_ThenEmailIsSentUsingTheDefaultCompanyInstanceEmailValues() throws Exception {
		when(mockUser.isActive()).thenReturn(true);
		when(mockUser.getEmailAddress()).thenReturn(RECIPIENT_ADDRESS);
		when(mockUser.getFullName()).thenReturn(RECIPIENT_NAME);
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_NAME)).thenReturn(FROM_SYSTEM_NAME);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_ADDRESS)).thenReturn(FROM_SYSTEM_ADDRESS);
		when(mockMailFactoryUtil.getInternetAddress(RECIPIENT_ADDRESS, RECIPIENT_NAME)).thenReturn(mockToInternetAddress);
		when(mockMailFactoryUtil.getInternetAddress(FROM_SYSTEM_ADDRESS, FROM_SYSTEM_NAME)).thenReturn(mockFromAdminInternetAddress);
		when(mockMailFactoryUtil.getMailMessage(mockFromAdminInternetAddress, mockToInternetAddress, SUBJECT, BODY)).thenReturn(mockMailMessage);

		mailServiceImpl.sendEmail(mockUser, SUBJECT, BODY);

		verify(mockMailService, times(1)).sendEmail(mockMailMessage);
	}

	@Test
	public void sendEmail_WithParameters_ThenEmailIsSentUsingTheDefaultCompanyInstanceEmailValues() throws Exception {
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_NAME)).thenReturn(FROM_CUSTOM_NAME);
		when(PrefsPropsUtil.getString(COMPANY_ID, PropsKeys.ADMIN_EMAIL_FROM_ADDRESS)).thenReturn(FROM_CUSTOM_ADDRESS);
		when(mockMailFactoryUtil.getInternetAddress(RECIPIENT_ADDRESS, RECIPIENT_NAME)).thenReturn(mockToInternetAddress);
		when(mockMailFactoryUtil.getInternetAddress(FROM_CUSTOM_ADDRESS, FROM_CUSTOM_NAME)).thenReturn(mockFromAdminInternetAddress);
		when(mockMailFactoryUtil.getMailMessage(mockFromAdminInternetAddress, mockToInternetAddress, SUBJECT, BODY)).thenReturn(mockMailMessage);

		mailServiceImpl.sendEmail(COMPANY_ID, RECIPIENT_ADDRESS, RECIPIENT_NAME, SUBJECT, BODY);

		verify(mockMailService, times(1)).sendEmail(mockMailMessage);
	}

	private void mockTermsAndConditions(boolean accepted, boolean required) {
		when(mockUser.isAgreedToTermsOfUse()).thenReturn(accepted);
		when(mockUser.getCompanyId()).thenReturn(COMPANY_ID);
		when(PrefsPropsUtil.getBoolean(COMPANY_ID, PropsKeys.TERMS_OF_USE_REQUIRED)).thenReturn(required);
	}
}
