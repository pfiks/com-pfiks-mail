/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.mail.util;

import java.io.UnsupportedEncodingException;

import javax.mail.internet.InternetAddress;

import org.osgi.service.component.annotations.Component;

import com.liferay.mail.kernel.model.MailMessage;

@Component(immediate = true, service = MailFactoryUtil.class)
public class MailFactoryUtil {

	public InternetAddress getInternetAddress(String address, String name) throws UnsupportedEncodingException {
		return new InternetAddress(address, name);
	}

	public MailMessage getMailMessage(InternetAddress from, InternetAddress to, String subject, String body) {
		return new MailMessage(from, to, subject, body, true);
	}
}
