/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.mail.service.impl;

import java.io.File;
import java.util.List;

import javax.mail.internet.InternetAddress;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.mail.kernel.model.MailMessage;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.Validator;
import com.pfiks.mail.exception.MailException;
import com.pfiks.mail.service.MailService;
import com.pfiks.mail.util.MailFactoryUtil;

@Component(immediate = true, service = MailService.class)
public class MailServiceImpl implements MailService {

	private static final Log LOG = LogFactoryUtil.getLog(MailServiceImpl.class);

	private com.liferay.mail.kernel.service.MailService mailService;
	private MailFactoryUtil mailFactoryUtil;

	@Override
	public void sendEmail(String toAddress, String toName, String fromAddress, String fromName, String subject, String body) throws MailException {
		sendEmail(toAddress, toName, fromAddress, fromName, subject, body, null);
	}

	@Override
	public void sendEmail(String toAddress, String toName, String fromAddress, String fromName, String subject, String body, List<File> attachments) throws MailException {
		try {
			InternetAddress toIA = mailFactoryUtil.getInternetAddress(toAddress, toName);
			InternetAddress fromIA = mailFactoryUtil.getInternetAddress(fromAddress, fromName);
			MailMessage mailMessage = mailFactoryUtil.getMailMessage(fromIA, toIA, subject, body);

			if (ListUtil.isNotEmpty(attachments)) {
				for (File file : attachments) {
					mailMessage.addFileAttachment(file);
				}
			}

			mailService.sendEmail(mailMessage);

			LOG.debug("Sent email to: " + toAddress + ", subject: " + subject);
		} catch (Exception e) {
			LOG.error("Exception sending email to: " + toAddress + ", subject: " + subject);
			throw new MailException(e);
		}
	}

	@Override
	public void sendEmail(User recipientUser, String fromAddress, String fromName, String subject, String body) throws MailException {
		if (isUserActive(recipientUser)) {
			sendEmail(recipientUser.getEmailAddress(), recipientUser.getFullName(), fromAddress, fromName, subject, body);
		}
	}

	@Override
	public void sendEmail(User recipientUser, String subject, String body) throws MailException {
		if (isUserActive(recipientUser)) {
			sendEmail(recipientUser.getCompanyId(), recipientUser.getEmailAddress(), recipientUser.getFullName(), subject, body);
		}
	}

	@Override
	public void sendEmail(String toAddress, String toName, String subject, String body) throws MailException {
		sendEmail(toAddress, toName, subject, body, null);
	}

	@Override
	public void sendEmail(String toAddress, String toName, String subject, String body, List<File> attachments) throws MailException {
		long companyId = CompanyThreadLocal.getCompanyId();
		sendEmail(companyId, toAddress, toName, subject, body, attachments);
	}

	@Override
	public void sendEmail(long companyId, String toAddress, String toName, String subject, String body) throws MailException {
		sendEmail(companyId, toAddress, toName, subject, body, null);
	}

	private void sendEmail(long companyId, String toAddress, String toName, String subject, String body, List<File> attachments) throws MailException {
		String fromName = PrefsPropsUtil.getString(companyId, PropsKeys.ADMIN_EMAIL_FROM_NAME);
		String fromAddress = PrefsPropsUtil.getString(companyId, PropsKeys.ADMIN_EMAIL_FROM_ADDRESS);
		sendEmail(toAddress, toName, fromAddress, fromName, subject, body, attachments);
	}

	private boolean isUserActive(User user) {
		if (Validator.isNotNull(user)) {
			boolean termsOfUseRequired = PrefsPropsUtil.getBoolean(user.getCompanyId(), PropsKeys.TERMS_OF_USE_REQUIRED);
			boolean termsOfUseAreRequiredAndAgreed = termsOfUseRequired && user.isAgreedToTermsOfUse();
			boolean agreedToTermsOfUse = !termsOfUseRequired || termsOfUseAreRequiredAndAgreed;
			return Validator.isNotNull(user) && user.isActive() && agreedToTermsOfUse;
		}
		return false;
	}

	@Reference(service = com.liferay.mail.kernel.service.MailService.class)
	protected void setMailService(com.liferay.mail.kernel.service.MailService mailService) {
		this.mailService = mailService;
	}

	@Reference(service = MailFactoryUtil.class)
	protected void setMailFactoryUtil(MailFactoryUtil mailFactoryUtil) {
		this.mailFactoryUtil = mailFactoryUtil;
	}

}
